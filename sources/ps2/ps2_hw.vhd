----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:30:27 07/16/2019 
-- Design Name: 
-- Module Name:    ps2_hw - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ps2_hw is
	 port (
		 clk         : in  std_logic;  -- System clock
		 reset       : in  std_logic;  -- System reset
		 oen         : in  std_logic;  -- Output enable to the data bus
		 ps2_clk     : in  std_logic;  -- Keyboard Clock Line
		 ps2_data    : in  std_logic;  -- Keyboard Data Line
		 irq         : out std_logic;  -- Interrupt signal
		 dataOut     : out std_logic_vector(15 downto 0) -- 16 bit data bus (high-Z while inactive)
	 );
end ps2_hw;

architecture Behavioral of ps2_hw is

COMPONENT ps2_module IS
	PORT (
		clk : IN STD_LOGIC;
		reset : IN STD_LOGIC; 
		ps2_clk : IN STD_LOGIC;
		ps2_data : IN STD_LOGIC;
		irq : OUT STD_LOGIC;
		scan_code : OUT STD_LOGIC_VECTOR(7 DOWNTO 0) 
	);
END COMPONENT;

COMPONENT ps2_keyboard_to_ascii IS
   PORT(
      clk          : IN  STD_LOGIC;                     --system clock input
      ps2_code_new : STD_LOGIC;                         --new PS2 code flag from ps2_keyboard component
      ps2_code     : STD_LOGIC_VECTOR(7 DOWNTO 0);      --PS2 code input form ps2_keyboard component
      ascii_new    : OUT STD_LOGIC;                     --output flag indicating new ASCII value
      ascii_code   : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)); --ASCII value
END COMPONENT;

signal clk_slow : STD_LOGIC := '0';
signal ps2_irq : STD_LOGIC := '0';
signal ps2_irq_deb : STD_LOGIC := '0';
signal scan_code : STD_LOGIC_VECTOR(7DOWNTO 0);
signal ascii_code : STD_LOGIC_VECTOR(6 DOWNTO 0);

begin
	PS2_module_1 : ps2_module
		PORT MAP (
			clk => clk,
			reset => reset,
			ps2_clk => ps2_clk,
			ps2_data => ps2_data,
			irq => ps2_irq,
			scan_code => scan_code	
		);	

	PS2_converter_1 : ps2_keyboard_to_ascii
		PORT MAP (
         clk => clk,
         ps2_code_new => ps2_irq_deb,
         ps2_code => scan_code,
         ascii_new => irq,
         ascii_code => ascii_code
		);	

ps2_irq_deb <= ps2_irq;
		
--	-- debouncing logic 
--	clk_slow <= debouncing_counter(7);
--	process (clk, reset)
--	begin
--	   if (reset = '1') then
--			debouncing_counter <= (others => '0');
--		elsif (clk'event and clk = '1') then
--			debouncing_counter <= debouncing_counter + 1;
--		end if;
--	end process;
--	
--	process (clk_slow)
--	begin
--	   if (reset = '1') then
--		   ps2_irq_deb <= '0';
--		elsif (clk_slow'event and clk_slow = '1') then
--			ps2_irq_deb <= ps2_irq;
--		end if;
--	end process;
	
	-- output
--	process (clk, reset)
--	begin
--	   if (reset = '1') then
--			dataOut (6 downto 0) <= (others => '0');
--		elsif (clk'event and clk = '1') then
--			if (oen = '1') then
--			   
--			end if;
--		end if;
--	end process;

	dataOut (6 downto 0) <= ascii_code;
	dataOut (15 downto 7) <= (others => '0');
	
end Behavioral;

