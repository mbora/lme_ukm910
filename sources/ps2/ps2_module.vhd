library IEEE;
  use IEEE.std_Logic_1164.all;
  use IEEE.numeric_std.all;

entity ps2_module is
  generic (
    filter_size : positive := 2
  );
  port (
    clk         : in  std_logic;  -- System clock
    reset       : in  std_logic;  -- System reset
    ps2_clk     : in  std_logic;  -- Keyboard Clock Line
    ps2_data    : in  std_logic;  -- Keyboard Data Line
    irq         : out std_logic;  -- Interrupt signal
    scan_code   : out std_logic_vector(7 downto 0) -- 16 bit data bus
  );

end ps2_module;

architecture FSM of ps2_module is
  signal  ps2_data_reg : std_logic;                     -- PS2 data register
  signal  clk_filter   : std_logic_vector(filter_size-1 downto 0);  -- PS2 clock shift register
  signal  ps2_clk_low  : std_logic;                     -- PS2 clock is low
  signal  fall_clk     : std_logic;                     -- PS2 clock has a falling edge
  signal  bit_cnt      : unsigned(3 downto 0);          -- Number of data bits processed
  signal  shift_reg    : std_logic_vector(8 downto 0);  -- 8 bits data + 1 bit parity (unchecked)
  
  constant clk_filter_all_one   : std_logic_vector(filter_size-1 downto 0) := (others => '1');
  constant clk_filter_all_zero  : std_logic_vector(filter_size-1 downto 0) := (others => '0');

  type   state_t is (IDLE, SHIFTING);
  signal state : state_t;
begin

  -- This process converts from asynchrouns communication to synchrouns communication
  process (clk, reset)
  begin
    if reset = '1' then
      ps2_data_reg  <= '0';
      ps2_clk_low   <= '0';
      clk_filter    <= clk_filter_all_zero;
      fall_clk      <= '0';
    elsif rising_edge(clk) then
      ps2_data_reg  <= ps2_data;
      fall_clk      <= '0';
      clk_filter    <= ps2_clk&clk_filter(clk_filter'high downto 1); -- shift register
      if clk_filter = clk_filter_all_one then
        ps2_clk_low <= '1'; -- clk_filter is stable and high
      elsif clk_filter = clk_filter_all_zero then
        ps2_clk_low <= '0'; -- clk_filter is stable and low
        if ps2_clk_low = '1' then
          fall_clk <= '1'; -- PS2 clk (filter) changed from 1 to 0
        end if;
      end if;
    end if;
  end process;


  -- This state machine reads in the serial data
  process (clk, reset)
  begin
    if reset = '1' then
      state         <= IDLE;
      bit_cnt       <= to_unsigned(0, 4);
      shift_reg     <= (others => '0');
      irq           <= '0';
    elsif rising_edge(clk) then
      case state is
        when IDLE =>
          bit_cnt <= to_unsigned(0, 4);
          irq     <= '0';
          if fall_clk = '1' and ps2_data_reg = '0' then -- Start bit
            state <= SHIFTING;
          end if;
        when SHIFTING =>
            if bit_cnt >= 9 then
              if fall_clk = '1' then -- Stop bit
                scan_code <= shift_reg(7 downto 0); -- exclude parity bit
                irq           <= '1';
                state         <= IDLE;
              end if;
            elsif fall_clk = '1' then -- Regular data (or parity) bit
              bit_cnt   <= bit_cnt + 1;
              shift_reg <= ps2_data_reg&shift_reg(shift_reg'high downto 1); -- Shift right
          end if;
        when others => -- never reached
          state <= IDLE;
      end case;
    end if;
  end process;

end FSM;

