----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/23/2019 06:56:40 PM
-- Design Name: 
-- Module Name: VGA_top_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY VGA_TB IS

END VGA_TB;

ARCHITECTURE Behavioral OF VGA_TB IS

COMPONENT VGA IS
    Port (  
        clk : IN  STD_LOGIC;
        res: IN  STD_LOGIC;
             
        oe : IN STD_LOGIC; 
        wren : IN STD_LOGIC;  
        
        addr : IN STD_LOGIC_VECTOR (1 downto 0);
        dataI : IN STD_LOGIC_VECTOR (15 downto 0);

        vsync : OUT STD_LOGIC;
        hsync : OUT STD_LOGIC;
        
        red : OUT STD_LOGIC;
        green : OUT STD_LOGIC;
        blue : OUT STD_LOGIC
    );
END COMPONENT;

SIGNAL clk : STD_LOGIC;
SIGNAL res : STD_LOGIC;
SIGNAL oe : STD_LOGIC;
SIGNAL wren : STD_LOGIC;
SIGNAL addr : STD_LOGIC_VECTOR (1 downto 0);
SIGNAL dataI : STD_LOGIC_VECTOR (15 downto 0);
SIGNAL vsync : STD_LOGIC;
SIGNAL hsync : STD_LOGIC;
SIGNAL red : STD_LOGIC;
SIGNAL blue : STD_LOGIC;
SIGNAL green : STD_LOGIC;
 
BEGIN

    VGA_MODULE : VGA
        PORT MAP (
            clk => clk,
            res => res,
            oe => oe,
            wren => wren,
            addr => addr,
            dataI => dataI,
            vsync => vsync,
            hsync => hsync,
            red => red,
            blue => blue,
            green => green
        );
        
    PROCESS
    BEGIN
    
       clk <= '0';
       WAIT FOR 5ns;
       
       clk <= '1'; 
       WAIT FOR 5ns;
       
    END PROCESS; 
    
    oe <= '0';
    wren <= '0';
    res <= '0';
    
END Behavioral;
