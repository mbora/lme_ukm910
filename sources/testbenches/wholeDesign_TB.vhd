----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 02:49:19 06/24/2019 
-- Design Name: UKM910 Processor
-- Module Name: wholeDesign_TB - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: IEEE.STD_LOGIC_1164
--
-- Revision: 2
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY wholeDesign_TB IS
END wholeDesign_TB;

ARCHITECTURE Behavioral OF wholeDesign_TB IS

COMPONENT wholeDesign IS
	PORT ( 
		-- control signals
		clk : IN STD_LOGIC;
		res : IN STD_LOGIC;
		-- inputs
--		interrupts : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		PS2clk : IN STD_LOGIC;
		PS2data : IN STD_LOGIC;
		-- outputs
		vsync : OUT STD_LOGIC;
		hsync : OUT STD_LOGIC;
		red : OUT STD_LOGIC;
		green : OUT STD_LOGIC;
		blue : OUT STD_LOGIC	
    );
END COMPONENT;

COMPONENT PS2Tester IS
	PORT (
		PS2clk  : OUT STD_LOGIC;
		PS2data : OUT STD_LOGIC 
	);
END COMPONENT;

SIGNAL clk : STD_LOGIC := '0';
SIGNAL res : STD_LOGIC := '0';
SIGNAL PS2clk : STD_LOGIC := '0';
SIGNAL PS2data : STD_LOGIC := '0';
SIGNAL vsync : STD_LOGIC := '0';
SIGNAL hsync : STD_LOGIC := '0';
SIGNAL red : STD_LOGIC := '0';
SIGNAL blue : STD_LOGIC := '0';
SIGNAL green : STD_LOGIC := '0';
SIGNAL interrupts : STD_LOGIC_VECTOR (7 DOWNTO 0) := (OTHERS => '0');

BEGIN

	wholeDesign_1 : wholeDesign 
		PORT MAP (
			clk => clk,
			res => res,
			PS2clk => PS2clk,
			PS2data => PS2data,
			vsync => vsync,
			hsync => hsync,
			red => red,
			green => green,
			blue => blue
--			interrupts => interrupts
		);

	PS2Tester_1 : PS2Tester
		PORT MAP (
			PS2clk => PS2clk,
			PS2data => PS2data		
		);
		
	createReset : PROCESS 
	BEGIN
		res <= '1';
		WAIT FOR 150 ns;
		res <= '0';
		WAIT;
	END PROCESS createReset;
	
	createClock : PROCESS
	BEGIN
		clk <= '0';
		WAIT FOR 100 ns;
		clk <= '1';
		WAIT FOR 100 ns;
	END PROCESS createClock;

	createInt : PROCESS
	BEGIN
		interrupts <= "00000000";
		WAIT FOR 10 us;
		interrupts <= "00000010";
		WAIT FOR 10 us;
		interrupts <= "00000000";
		WAIT FOR 130 us;
	END PROCESS createInt;
	
END Behavioral;

