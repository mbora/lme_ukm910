-- Vhdl test bench created from schematic C:\Daten\ISE\IntegrierteSchaltungenProjekt\UKM801.sch - Wed Jan 09 22:17:10 2008
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;

ENTITY UKM910_TB IS
END UKM910_TB;

	ARCHITECTURE behavioral OF UKM910_TB IS 

   COMPONENT UKM910
   PORT( 
		oe	:	OUT	STD_LOGIC; 
		wren	:	OUT	STD_LOGIC; 
		res	:	IN	STD_LOGIC; 
		clk	:	IN	STD_LOGIC; 
		address	:	OUT	STD_LOGIC_VECTOR (11 DOWNTO 0);
		dataI	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		interrupts : IN  STD_LOGIC_VECTOR (7 DOWNTO 0)
		);
   END COMPONENT;
	
	COMPONENT memory
	PORT(
		clk : IN std_logic;
		address : IN std_logic_vector(10 downto 0);
		wren : IN std_logic;
--		oe : IN std_logic;       
		dataI : IN std_logic_vector(15 downto 0);
		dataO : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	SIGNAL oe	:	STD_LOGIC;
   SIGNAL wren	:	STD_LOGIC;
   SIGNAL res	:	STD_LOGIC;
   SIGNAL clk	:	STD_LOGIC;
   SIGNAL address	:	STD_LOGIC_VECTOR (11 DOWNTO 0);
   SIGNAL dataMem	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL dataUKM : STD_LOGIC_VECTOR (15 DOWNTO 0);
	SIGNAL interrupts : STD_LOGIC_VECTOR (15 DOWNTO 0);
	
BEGIN

   UUT: UKM910 PORT MAP(
		oe => oe, 
		wren => wren, 
		res => res, 
		clk => clk, 
		address => address, 
		dataI => dataMem,
		dataO => dataUKM,
		interrupts => interrupts
   );
	
	mem: memory PORT MAP(
		clk => clk,
		address => address(10 downto 0),
		dataI => dataUKM,
		dataO => dataMem,
		wren => wren
--		oe => oe 
	);
	
	createReset : PROCESS 
	BEGIN
		res <= '1';
		WAIT FOR 150 ns;
		res <= '0';
		WAIT;
	END PROCESS createReset;
	
	createClock : PROCESS
	BEGIN
		clk <= '0';
		WAIT FOR 100 ns;
		clk <= '1';
		WAIT FOR 100 ns;
	END PROCESS createClock;

END;
