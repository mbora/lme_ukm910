
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_tb is
end ALU_tb;

architecture behaviour of ALU_tb is

    component ALU is
        port (
            A, B                : in  std_logic_vector(15 downto 0);
            C                   : out std_logic_vector(15 downto 0);
            ALUFunc             : in  std_logic_vector(3 downto 0);
            nBit, shiftrot, dir : in  std_logic;
            n, z, cout          : out std_logic );
    end component;

    signal A : std_logic_vector(15 downto 0) := x"0005";
    signal B : std_logic_vector(15 downto 0) := x"0002";
    signal C : std_logic_vector(15 downto 0) := x"FFFF";
    signal ALUFunc     : std_logic_vector(3 downto 0) := "0000";
    signal nBit        : std_logic := '0';
    signal shiftrot    : std_logic := '0';
    signal dir         : std_logic := '0';
    signal cout, n, z  : std_logic := '0';

begin

    uut: ALU port map(
        A => A,
        B => B,
        C => C,
        ALUFunc => ALUFunc,
        nBit => nBit,
        shiftrot => shiftrot,
        dir => dir,
        cout => cout,
        n => n,
        z => z );

    process
    begin
        ALUFunc <= "0000";
        wait for 10 ns;
        assert C = x"0000" report "0000: C = 0 failed";
        assert cout = '0' report "cout = 0 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0001";
        wait for 10 ns;
        assert C = x"0007" report "0001: C = A + B failed";
        assert cout = '0' report "cout = 0 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '0' report "z = 0 failed";

        ALUFunc <= "0010";
        wait for 10 ns;
        assert C = x"0003" report "0010: C = A - B failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '0' report "z = 0 failed";

        ALUFunc <= "0011";
        wait for 10 ns;
        assert C = std_logic_vector(- x"0003") report "0011: C = B - A failed";
        assert cout = '0' report "cout = 0 failed";
        assert n = '1' report "n = 1 failed";
        assert z = '0' report "z = 0 failed";

        ALUFunc <= "0100";
        wait for 10 ns;
        assert C = A report "0100: C = A failed";

        ALUFunc <= "0101";
        wait for 10 ns;
        assert C = B report "0101: C = B failed";

        ALUFunc <= "0110";
        wait for 10 ns;
        assert C = x"0006" report "0110: C = A + 1 failed";

        ALUFunc <= "0111";
        wait for 10 ns;
        assert C = x"0003" report "0111: C = B + 1 failed";

        ALUFunc <= "1000";
        wait for 10 ns;
        assert C = (A and B) report "1000: C = A AND B failed";

        ALUFunc <= "1001";
        wait for 10 ns;
        assert C = (A or B) report "1001: C = A OR B failed";

        ALUFunc <= "1010";
        wait for 10 ns;
        assert C = (A xor B) report "1010: C = A XOR B failed";

        -- 1011: don't care

        ALUFunc <= "1100";
        wait for 10 ns;
        assert C = not A report "1010: C = NOT A failed";

        ALUFunc <= "1101";
        wait for 10 ns;
        assert C = not B report "1011: C = NOT B failed";

        ALUFunc <= "1110";
        wait for 10 ns;
        assert C = std_logic_vector(signed(not A) + 1)
            report "1110: C = (NOT A) + 1 failed";

        ALUFunc <= "1111";
        wait for 10 ns;
        assert C = std_logic_vector(signed(not B) + 1)
            report "1111: C = (NOT B) + 1 failed";

        ALUFunc <= "0001";
        A <= "1111111111111111";
        B <= "0000000000000001";
        wait for 10 ns;
        assert C = "0000000000000000" report "A + B: overflow failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0010";
        A <= "1111111111111111";
        B <= "1111111111111111";
        wait for 10 ns;
        assert C = "0000000000000000" report "A - B: overflow failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0011";
        A <= "1111111111111111";
        B <= "1111111111111111";
        wait for 10 ns;
        assert C = "0000000000000000" report "B - A: overflow failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0110";
        wait for 10 ns;
        assert C = "0000000000000000" report "A + 1: overflow failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0111";
        wait for 10 ns;
        assert C = "0000000000000000" report "B + 1: overflow failed";
        assert cout = '1' report "cout = 1 failed";
        assert n = '0' report "n = 0 failed";
        assert z = '1' report "z = 1 failed";

        ALUFunc <= "0000";
        wait;

    end process;

end;
