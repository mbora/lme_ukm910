----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 18:00:05 06/29/2019 
-- Design Name: UKM910 Processor
-- Module Name: IntHandler - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: STD_LOGIC_1164
--
-- Revision: 3
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY IFlagREG IS
	PORT (
		clk : IN STD_LOGIC;
		res : IN STD_LOGIC;
		en : IN STD_LOGIC;
		resIFLAG : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		interrupts : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		d : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END IFlagREG;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ARCHITECTURE Behavioral OF IFlagREG IS

SIGNAL q_async : STD_LOGIC_VECTOR (7 DOWNTO 0);
SIGNAL q_sync : STD_LOGIC_VECTOR (15 DOWNTO 0);

BEGIN		-- Behavioral

	-- generates 7 flip-flops for the interrupts 
	FF: FOR i IN 0 TO 7 GENERATE
		PROCESS (interrupts(i), resIFLAG(i), res) IS
		BEGIN
			-- 7 asynchronous reset signals 
			IF ((res OR resIFLAG(i)) = '1') THEN				
				q_async(i) <= '0';
			-- 7 individual set signals (sensitive to interrupts) 
			ELSIF (interrupts(i)'EVENT AND interrupts(i) = '1') THEN
				-- input interrupts are stored in asynchronous flipflops
				q_async(i) <= '1';
			END IF;
		END PROCESS;
	END GENERATE;
	
	-- synchronous register
	PROCESS (clk, res)
	BEGIN
		-- asynchronous reset
		IF (res = '1') THEN
			q_sync <= (OTHERS => '0');
		-- synchronous with falling edge
		ELSIF (clk'EVENT AND clk = '1') THEN
			-- explained in detail below *
			q_sync(7 DOWNTO 0) <= (q_sync(7 DOWNTO 0) OR q_async) AND NOT(resIFLAG);
			IF (en = '1') THEN
				q_sync <= d;
			END IF;
		END IF;
	END PROCESS;
	-- output is obtained from synchronous register
	q <= q_sync;
	
END Behavioral;

-- *	The flags stored in the asynchronous flipflops are OR(ed) with the current flags stored in the synchronus
-- 	one. If one of the interrupts is high, the corresponding flag is set. To reset the individual flag, the
--		control unit sends 7-bit reset signal corresponding to the flag. In the synchronus register the (AND NOT) 
--		uses the 7-bit reset signal as a mask to reset the individual flags in the synchronous register. For
-- 	example, to reset 2nd flag, the resIFLAG would be 00000010 and its complement is 11111101. When it is
--		(AND)ed with the current value, the second flag is cleared leaving the rest unchanged