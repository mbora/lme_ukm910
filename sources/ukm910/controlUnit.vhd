----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 21:50:17 05/27/2019 
-- Design Name: UKM910 Processor
-- Module Name: controlUnit - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description:	 
--
-- Dependencies: IEEE.NUMERIC_STD
--
-- Revision: 12
-- Revision 0.01 - File Created
--
-- Additional Comments
--
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY controlUnit IS
	PORT ( 
		-- control unit inputs
		clk, res : IN STD_LOGIC;
		opcode : IN STD_LOGIC_VECTOR (10 DOWNTO 0);
		flags : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		IFLAG : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		IEN : IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		
		-- enable signals
		enPC : OUT STD_LOGIC;
		enIR : OUT STD_LOGIC;
		enACC : OUT STD_LOGIC;
		enReg : OUT STD_LOGIC;
		enGIE : OUT STD_LOGIC;
		oe, wren : OUT STD_LOGIC;
		
		-- mux selection signals
		selPC : OUT STD_LOGIC;
		selB : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
		selAddr : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
		selReg : OUT STD_LOGIC_VECTOR (2 DOWNTO 0);
		ALUfunc : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		nsd : OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);

		-- other control signals
		GIEflag : OUT STD_LOGIC;
		enFlags : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		resIFLAG : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
		addrINT : OUT STD_LOGIC_VECTOR (2 DOWNTO 0)
	);
END controlUnit;

ARCHITECTURE Behavioral OF controlUnit IS

-- finite state machine state decleration
TYPE state IS (fetch, decode, add, sub, bitwiseAnd, bitwiseNot, comp, rotr, shr, 
	load, loadr, loadi, storer, storei, inc, dec, ret, reti, call, jump, busy);

-- signal decleration

SIGNAL currentState : state := busy; 
SIGNAL nextState : state; 

SIGNAL foundINT : STD_LOGIC := '0';
SIGNAL processINT : STD_LOGIC := '0';
SIGNAL enProcessINT  : STD_LOGIC := '0';
SIGNAL enAddrINT : STD_LOGIC := '0';

SIGNAL intIndex : INTEGER RANGE 0 TO 7;

BEGIN		-- Behavioral

	-- finite state machine
	PROCESS (clk, res)
	BEGIN
		IF (res = '1') THEN
			currentState <= busy;				
		ELSE
			IF (clk'EVENT AND clk = '1') THEN
				currentState <= nextState;
				-- register to hold the interrupt address
				IF (enAddrINT = '1') THEN
					addrInt <= STD_LOGIC_VECTOR(TO_UNSIGNED(intIndex,3));
				END IF;
				-- flipflop to hold the state of the interrupt being processed
				IF (enProcessINT = '1') THEN
					ProcessInt <= foundINT;
				END IF;
			END IF;
		END IF;
	END PROCESS;
	
	-- finite state machine decoder
	PROCESS (currentState, opcode, flags, IFLAG, IEN, processInt) 
	
	VARIABLE opcode1 : STD_LOGIC_VECTOR (3 DOWNTO 0);
	VARIABLE opcode2 : STD_LOGIC_VECTOR (1 DOWNTO 0);
	VARIABLE opcode3 : STD_LOGIC_VECTOR (1 DOWNTO 0);
	VARIABLE opcode4 : STD_LOGIC_VECTOR (2 DOWNTO 0);
	
	BEGIN
		
		-- default values
		
		opcode1 := opcode(10 DOWNTO 7);	-- IR[15:11] : function
		opcode2 := opcode(6 DOWNTO 5);	-- IR[9:8] : sub-funtion
		opcode3 := opcode(4 DOWNTO 3);	-- IR[5:4] : sub-funtion
		opcode4 := opcode(2 DOWNTO 0);	-- IR[2:0] : reg. select
		
		enPC <= '0';			-- all registers disabled
		enIR <= '0';
		enACC <= '0';
		enReg <= '0';
		enGIE <= '0';
		
		oe <= '1';				-- memory read enabled
		wren <= '0';			-- memory write disabled		

		selPC <= '0';			-- PC from ALU result
		selB <= "10";			-- data from memory
		selAddr <= "01";		-- address from PC	
		selReg <= opcode4;	-- selection from IR
		
		ALUfunc <= "0000";	-- ALU 0
		nsd <= "000";			-- no shift
		
		enFlags <= "0000";	-- flags unchanged
		
		resIFLAG <= X"00";	-- no IFLAG reset
		GIEflag <= '0';		-- GIEflag not set

		foundINT <= '0';		-- no interrupts to process
		enProcessINT <= '0';	-- status flipflop disabled
		
		intIndex <= 0;			-- interrupt number
		enAddrINT <= '0';		-- AddrINT register disabled
		
		-- next state and output generation	
		CASE currentState IS	
			
			WHEN fetch =>

				-- normal fetch (no interrupts)
				nextState <= decode;
				enProcessINT <= '1';		-- update the state
				enIR <= '1';				-- IR enabled				
				ALUfunc <= "0101";		-- ALU B
				
				-- interrupt handler
				IF (IEN(8) = '1') THEN
					FOR i IN 0 TO 7 LOOP
						IF ((IEN(i) AND IFLAG(i)) = '1') THEN
							nextState <= dec;
							foundINT <= '1';			-- interrupt found
							intIndex <= i;				-- interrupt number
							enAddrINT <= '1';			-- update interrupt address
							resIFLAG(i) <= '1';		-- reset interrupt flag
							GIEflag <= '0';			-- GIE flag value
							enGIE <= '1';				-- update GIE flag
							enIR <= '0';				-- disable IR
							EXIT;							
						END IF;
					END LOOP;
				END IF;
				
			WHEN decode =>	
			
			   enPC <= '1';				-- PC enabled	
				selB <= "01";				-- data from pc
				ALUfunc <= "0111";		-- ALU B + 1					
				
				CASE (opcode1) IS
							
					WHEN "0001" => -- add
					
						nextState <= add;
						selAddr <= "00";				-- address from IR
						
					WHEN "0010" => -- sub
					
						nextState <= sub;
						selAddr <= "00";				-- address from IR
						
					WHEN "0011" => -- and
					
						nextState <= bitwiseAnd;
						selAddr <= "00";				-- address from IR
						
					WHEN "0100" =>	-- not
					
						nextState <= bitwiseNot;
						selAddr <= "00";				-- address from IR
						
					WHEN "0101" =>	-- comp
					
						nextState <= comp;
						selAddr <= "00";				-- address from IR
					
					WHEN "0110" => -- rotr	
					
						nextState <= rotr;
						selAddr <= "00";				-- address from IR
					
					WHEN "0111" =>	-- shr
					
						nextState <= shr;
						selAddr <= "00";				-- address from IR
					
					WHEN "1000" =>	-- load
					
						nextState <= load;
						selAddr <= "00";				-- address from IR

					WHEN "1001" => -- store
					
						nextState <= busy;	
						oe <= '0';						-- memory read disabled
						wren <= '1';					-- memory write enabled		
						selAddr <= "00";				-- address from IR
						
					WHEN "1010" =>	-- loadr / loadi / loadiinc / loadidec / ret /reti
						
						IF (opcode2 = "00") THEN
							CASE (opcode3) IS
								
								WHEN "00" => -- loadr
									nextState <= loadr;
											
								WHEN "11" => -- loadidec
									nextState <= dec;
									
								WHEN OTHERS => -- loadi / loadiinc
									nextState <= loadi;
									selAddr <= "10";		-- address from regFile	
									
							END CASE;		
							
						ELSE	-- ret / reti
							
							nextState <= ret;
							enPC <= '0';			-- PC disabled	
							selAddr <= "10";		-- address from regFile	
							
						END IF;
					
					WHEN "1011" =>	-- storer / storei / storeiinc / storeidec
					
						CASE (opcode3) IS
							
							WHEN "00" => -- storer
								nextState <= storer;
										
							WHEN "11" => -- storeidec
								nextState <= dec;
								
							WHEN OTHERS => -- storei / storeiinc
												
								oe <= '0';						-- memory read disabled
								wren <= '1';					-- memory write enabled		
								selAddr <= "10";				-- address from RegFile
								
								IF (opcode3(0) = '0') THEN	-- storeiinc
									nextState <= inc;
								ELSE
									nextState <= busy;
								END IF;
								
						END CASE;				

					WHEN "1100" =>	-- jump
					
						nextState <= fetch;
						selAddr <= "11";				-- address from ALU
						selB <= "00";					-- data from IR
						ALUfunc <= "0101";			-- ALU B						
							
					WHEN "1101" =>	-- bz	
						
						nextState <= fetch;
						selAddr <= "11";				-- address from ALU
						
						IF (flags(0) = '1') THEN	-- branch					
							selB <= "00";				-- data from IR
							ALUfunc <= "0101";		-- ALU B	
						END IF;
														
					WHEN "1110" =>	-- bn	
						
						nextState <= fetch;
						selAddr <= "11";				-- address from ALU
						
						IF (flags(1) = '1') THEN	-- branch					
							selB <= "00";				-- data from IR
							ALUfunc <= "0101";		-- ALU B		
						END IF;
						
					WHEN "1111" =>	-- call
			
						nextState <= call;
						selB <= "11";					-- data from ALU
						selReg <= "000";				-- SP
						ALUfunc <= "1011";			-- ALU B - 1					
						enReg <= '1';					-- RegFile enabled
						enPC <= '0';					-- PC disabled
						
					WHEN OTHERS => -- nop
						nextState <= fetch;
						selAddr <= "11";				-- address from ALU
						
				END CASE;
			
			WHEN add =>

				nextState <= fetch;		
				enACC <= '1';				-- ACC enabled
				enFlags <= "1111";		-- update ov, c, n, z
				ALUfunc <= "0001";		-- ALU A + B			
				
			WHEN sub =>

				nextState <= fetch;			
				enACC <= '1';				-- ACC enabled
				enFlags <= "1111";		-- update ov, c, n, z
				ALUfunc <= "0010";		-- ALU A - B	
				
			WHEN bitwiseAnd =>

				nextState <= fetch;						
				enACC <= '1';				-- ACC enabled
				enFlags <= "0011";		-- update n and z
				ALUfunc <= "1000";		-- ALU A & B	

			WHEN bitwiseNot =>
	
				nextState <= fetch;
				enACC <= '1';				-- ACC enabled
				enFlags <= "0011";		-- update n and z
				ALUfunc <= "1100";		-- ALU !(A)		
			
			WHEN comp =>

				nextState <= fetch;
				enACC <= '1';				-- ACC enabled
				enFlags <= "1111";		-- update ov, c, n, z
				ALUfunc <= "1110";		-- ALU !(A)+1					

			WHEN rotr =>
				
				nextState <= fetch;
				enACC <= '1';				-- ACC enabled
				enFlags <= "0011";		-- update n and z
				ALUfunc <= "0100";		-- ALU A
				nsd <= "110";				-- rotate right			
			
			WHEN shr =>
			
				nextState <= fetch;
				enACC <= '1';				-- ACC enabled
				enFlags <= "0011";		-- update n and z
				ALUfunc <= "0100";		-- ALU A
				nsd <= "100";				-- shift right

			WHEN load =>
			
				nextState <= fetch;
				enACC <= '1';				-- ACC enabled
				enFlags <= "0011";		-- update n and z
				ALUfunc <= "0101";		-- ALU B
													
			WHEN loadr =>
				
				nextState <= fetch;		
				enFlags <= "0011";		-- update n and z						
				enACC <= '1';				-- ACC enabled
				selB <= "11";				-- data from RegFile
				ALUfunc <= "0101";		-- ALU B			
									
			WHEN loadi =>	
			
				enFlags <= "0011";		-- update n and z										
				enACC <= '1';				-- ACC enabled
				selB <= "10";				-- data from memory
				ALUfunc <= "0101";		-- ALU B	
				
				IF (opcode3(0) = '0') THEN	-- loadiinc
					nextState <= inc;		
				ELSE
					nextState <= fetch;
				END IF;

			WHEN ret =>
			
				nextState <= inc;
				ALUfunc <= "0101";		-- ALU B
				enPC <= '1';				-- PC enabled
			
			WHEN reti =>
 			
				nextState <= fetch;
				GIEflag <= '1';			-- set GIE
				enGIE <= '1';				-- enable IEN

			WHEN storer =>
			
				nextState <= fetch;					
				ALUfunc <= "0100";		-- ALU A
				enReg <= '1';				-- RegFile enabled
			
			WHEN storei =>
			
				oe <= '0';					-- memory read disabled
				wren <= '1';				-- memory write enabled		
				selAddr <= "10";			-- address from RegFile
				
				IF (opcode1 = "1111" OR processInt = '1') THEN	-- call
					nextState <= jump;
					selReg <= "000";		-- SP
				ELSE
					nextState <= busy;
				END IF;
				
			WHEN inc =>

				selB <= "11";				-- data from regFile
				ALUfunc <= "0111";		-- ALU B + 1					
				enReg <= '1';				-- RegFile enabled	
				
				IF (opcode2 = "10") THEN
					nextState <= reti;
				ELSE
					nextState <= fetch;							
				END IF;
				
			WHEN dec =>
			
				selAddr <= "11";			-- address from ALU
				selB <= "11";				-- data from regFile
				ALUfunc <= "1011";		-- ALU B - 1					
				enReg <= '1';				-- RegFile enabled
				
				IF (processInt = '1') THEN
					selReg <= "000";			-- SP
					nextState <= call;
				ELSE
					IF(opcode1 = "1010") THEN	
						nextState <= loadi;
					ELSE
						nextState <= storei;
					END IF;
				END IF;
			
			WHEN call =>

				nextState <= storei;
				enACC <= '1';				-- ACC enabled
				selB <= "01";				-- data from PC			

				IF (processInt = '1') THEN
				-- store the current PC in case of interrupt
					ALUfunc <= "0101";	-- ALU B
				ELSE
				-- store the next PC in case of call
					ALUfunc <= "0111";	-- ALU B	+ 1	
				END IF;
				
			WHEN jump => 
			
				nextState <= fetch;		
				selAddr <= "11";			-- address from interrupt
				selB <= "00";				-- data from IR
				ALUfunc <= "0101";		-- ALU B	
				enPC <= '1';				-- enable PC
				
				IF (processInt = '1') THEN
					selPC <= '1';			-- PC from addrINT
				END IF;
				
			WHEN OTHERS =>	
				-- if any instructions to be implemented
				nextState <= fetch;
				
		END CASE; 
	END PROCESS;
	
END Behavioral;