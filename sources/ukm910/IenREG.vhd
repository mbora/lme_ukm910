----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 12:25:49 07/06/2019 
-- Design Name: UKM910 Processor
-- Module Name: PswREG - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: STD_LOGIC_1164
--
-- Revision: 2
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY IenREG IS
	PORT (
		clk, res, enData : IN STD_LOGIC;
		GIEflag, enGIE : IN STD_LOGIC;
		d : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END IenREG;

ARCHITECTURE Behavioral of IenREG is

BEGIN		-- Behavioral
	
	PROCESS (clk, res)
	BEGIN
		-- asynchronous clear on reset
		IF (res = '1') THEN
			q <= (OTHERS => '0');
		-- synchronous write (rising edge)
		ELSIF (clk'EVENT AND clk = '1') THEN
			-- store data when enData
			IF (enData = '1') THEN
				q <= d;
			-- update GIEflag when enGIE
			ELSIF (enGIE = '1') THEN
				q(8) <= GIEflag;
			END IF;
		END IF;
	END PROCESS;

END Behavioral;

