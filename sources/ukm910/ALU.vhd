----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 01:39:44 05/16/2019 
-- Design Name: UKM910 Processor
-- Module Name: ALU - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description:	 
--
-- Dependencies: IEEE.NUMERIC_STD
--
-- Revision: 0
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY ALU IS
	PORT ( 
		
		-- i/o data ports
		A : IN  STD_LOGIC_VECTOR (15 downto 0);
		B : IN  STD_LOGIC_VECTOR (15 downto 0);
		C : OUT  STD_LOGIC_VECTOR (15 downto 0);
		
		-- operation selection
		ALUfunc : IN  STD_LOGIC_VECTOR (3 downto 0);
		
		-- shift selection
		nBit : IN  STD_LOGIC;
		shiftrot : IN  STD_LOGIC;
		dir : IN  STD_LOGIC;
		
		-- flags
		cout : OUT  STD_LOGIC;
		z : OUT  STD_LOGIC;
		n : OUT  STD_LOGIC;
		ov : OUT STD_LOGIC
	);
END ALU;

ARCHITECTURE Behavioral OF ALU IS

-- signal decleration

SIGNAL tmpA : STD_LOGIC_VECTOR (15 DOWNTO 0);
SIGNAL tmpB : STD_LOGIC_VECTOR (15 DOWNTO 0);
SIGNAL tmpResult : STD_LOGIC_VECTOR (16 DOWNTO 0);
SIGNAL result : STD_LOGIC_VECTOR (15 DOWNTO 0);
SIGNAL shiftFunc : STD_LOGIC_VECTOR (1 DOWNTO 0);

BEGIN		-- begin Behavioral

	-- shift function generation
	
	shiftFunc <= nBit & dir;

	-- main ALU operations
		
	WITH ALUfunc SELECT tmpResult <=
		(OTHERS => '0') WHEN "0000",
		STD_LOGIC_VECTOR(UNSIGNED('0' & A) + UNSIGNED('0' & B)) WHEN "0001",
		STD_LOGIC_VECTOR(UNSIGNED('0' & A) + UNSIGNED('0' & (NOT B)) + 1) WHEN "0010",
		STD_LOGIC_VECTOR(UNSIGNED('0' & B) + UNSIGNED('0' & (NOT A)) + 1) WHEN "0011",
		'0' & A WHEN "0100",
		'0' & B WHEN "0101",
		STD_LOGIC_VECTOR(UNSIGNED('0' & A) + 1) WHEN "0110",
		STD_LOGIC_VECTOR(UNSIGNED('0' & B) + 1) WHEN "0111",	
		'0' & (A AND B) WHEN "1000",
		'0' & (A OR B) WHEN "1001",
		'0' & (A XOR B) WHEN "1010",
		STD_LOGIC_VECTOR(UNSIGNED('0' & B) - 1) WHEN "1011",
		'0' & (NOT A) WHEN "1100",
		'0' & (NOT B) WHEN "1101",
		STD_LOGIC_VECTOR(UNSIGNED('0' & NOT(A)) + 1) WHEN "1110",
		STD_LOGIC_VECTOR(UNSIGNED('0' & NOT(B)) + 1) WHEN OTHERS;
		
	-- shift operations
	
	WITH shiftFunc SELECT result <=
		(tmpResult(0) AND shiftrot) & tmpResult(15 DOWNTO 1) WHEN "10",
		tmpResult(14 DOWNTO 0) & (tmpResult(15) AND shiftrot) WHEN "11",
		tmpResult(15 DOWNTO 0) WHEN OTHERS;
	
	-- result and flags generation

	tmpA <= STD_LOGIC_VECTOR(UNSIGNED(NOT A) + 1) WHEN ALUfunc = "0011" ELSE A;			
	tmpB <= STD_LOGIC_VECTOR(UNSIGNED(NOT B) + 1) WHEN ALUfunc = "0010" ELSE B;
	
	C <= result;
	
	OV <= (tmpA(15) XOR tmpResult(15)) AND (tmpB(15) XOR tmpResult(15));
	cout <= tmpResult(16);
	n <= result(15);
	z <= '1' WHEN result(15 DOWNTO 0) = (15 DOWNTO 0 => '0') ELSE '0';	
		
END Behavioral;

