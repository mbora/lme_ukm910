----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 20:08:44 05/27/2019 
-- Design Name: UKM910 Processor
-- Module Name: register - Behavioral 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: IEEE.NUMERIC_STD
--
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY REG IS
	PORT ( 
		clk, res, en: IN  STD_LOGIC;
		d : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT  STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END REG;

ARCHITECTURE Behavioral OF REG IS

BEGIN		-- Behavioral

	PROCESS (clk, res, en) IS
	BEGIN	
		-- asynchronous clear on reset
		IF (res = '1') THEN				
			q <= (OTHERS => '0');
		-- synchronous write (rising edge)
		ELSIF (clk'EVENT AND clk = '1') THEN
			-- store data when enabled
			IF (en = '1') THEN
				q <= d;
			END IF;
		END IF;
	END PROCESS;
END Behavioral;

