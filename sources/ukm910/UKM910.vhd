----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 19:27:58 05/27/2019 
-- Design Name: UKM910 Processor
-- Module Name: UKM910 - structure 
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: IEEE.NUMERIC_STD
--
-- Revision: 9
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY UKM910 IS
	PORT (
		clk : IN  STD_LOGIC;
		res : IN  STD_LOGIC;
		dataI : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		address : OUT  STD_LOGIC_VECTOR (11 DOWNTO 0);
		interrupts : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		wren : OUT  STD_LOGIC;
		oe : OUT  STD_LOGIC
	);
END UKM910;

ARCHITECTURE structure OF UKM910 IS

-- component declarations

COMPONENT ALU IS
	PORT ( 
		A, B : IN  STD_LOGIC_VECTOR (15 downto 0);
		C : OUT  STD_LOGIC_VECTOR (15 downto 0);
		ALUfunc : IN  STD_LOGIC_VECTOR (3 downto 0);
		nBit, shiftrot, dir : IN  STD_LOGIC;
		cout, z, n, ov : OUT  STD_LOGIC
	);
END COMPONENT;

COMPONENT REG IS
	PORT ( 
		clk, res, en : IN  STD_LOGIC;
		d : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT  STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT PswREG IS
	PORT (
		clk, res, enData : IN STD_LOGIC;
		enFlags : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		vcnz : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		d : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT IenREG IS
	PORT (
		clk, res, enData : IN STD_LOGIC;
		enGIE, GIEflag : IN STD_LOGIC;
		d : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT IFlagREG IS
	PORT ( 
		clk, res, en: IN STD_LOGIC;
		resIFLAG : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		interrupts : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		d : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		q : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT controlUnit IS
	PORT ( 
		-- control unit inputs
		clk, res : IN STD_LOGIC;
		opcode : IN STD_LOGIC_VECTOR (10 DOWNTO 0);
		flags : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		IFLAG : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		IEN : IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		-- enable signals
		enPC : OUT STD_LOGIC;
		enIR : OUT STD_LOGIC;
		enACC : OUT STD_LOGIC;
		enReg : OUT STD_LOGIC;
		enGIE : OUT STD_LOGIC;
		oe, wren : OUT STD_LOGIC;
		-- mux selection signals
		selPC : OUT STD_LOGIC;
		selB : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
		selAddr : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
		selReg : OUT STD_LOGIC_VECTOR (2 DOWNTO 0);
		ALUfunc : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		nsd : OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		-- other control signals
		GIEflag : OUT STD_LOGIC;
		enFlags : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		resIFLAG : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
		addrINT : OUT STD_LOGIC_VECTOR (2 DOWNTO 0)
	);
END COMPONENT;

-- data signals
SIGNAL dataPC : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');
SIGNAL dataIR : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');
SIGNAL dataACC : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');
SIGNAL dataSP : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataPTR1 : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataPTR2 : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataPTR3 : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataPSW : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');
SIGNAL dataIEN : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataIFLAG : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataReg : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL dataB : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL result : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');
SIGNAL nextAddr : STD_LOGIC_VECTOR (15 downto 0) := (OTHERS => '0');

-- enable signals
SIGNAL enPC : STD_LOGIC := '0';
SIGNAL enIR : STD_LOGIC := '0';
SIGNAL enACC : STD_LOGIC := '0';
SIGNAL enReg : STD_LOGIC := '0';
SIGNAL enGIE : STD_LOGIC := '0';
SIGNAL enFlags : STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '0');
SIGNAL enRegFile : STD_LOGIC_VECTOR (6 DOWNTO 0) := (OTHERS => '0');

-- multiplexer selection signals
SIGNAL selPC : STD_LOGIC := '0';
SIGNAL selAddr : STD_LOGIC_VECTOR (1 DOWNTO 0) := (OTHERS => '0');
SIGNAL selReg : STD_LOGIC_VECTOR (2 DOWNTO 0) := (OTHERS => '0');
SIGNAL selB : STD_LOGIC_VECTOR (1 DOWNTO 0) := (OTHERS => '0');
SIGNAL ALUfunc : STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '0');
SIGNAL opcode : STD_LOGIC_VECTOR (10 DOWNTO 0) := (OTHERS => '0');
SIGNAL nsd : STD_LOGIC_VECTOR (2 DOWNTO 0) := (OTHERS => '0');

-- other control signals
SIGNAL addrINT : STD_LOGIC_VECTOR (2 DOWNTO 0) := (OTHERS => '0');
SIGNAL resIFLAG : STD_LOGIC_VECTOR (7 DOWNTO 0) := (OTHERS => '0');
SIGNAL vcnz : STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '0');
SIGNAL flags : STD_LOGIC_VECTOR (1 downto 0) := (OTHERS => '0');
SIGNAL GIEflag : STD_LOGIC := '0';

BEGIN		-- structural

	-- component installation

	PC: REG PORT MAP ( clk => clk, res => res, 
			en => enPC,	d => nextAddr, q => dataPC );
		
	IR: REG PORT MAP ( clk => clk, res => res,
			en => enIR, d => result, q => dataIR );
	 
	ACC: REG PORT MAP ( clk => clk, res => res,
			en => enACC, d => result, q => dataACC	);
			
	SP: REG PORT MAP ( clk => clk, res => res,
			en => enRegFile(0), d => result,	q => dataSP	);
			
	PTR1: REG PORT MAP (	clk => clk,	res => res,	
			en => enRegFile(1), d => result, q => dataPTR1 );
	
	PTR2: REG PORT MAP (	clk => clk, res => res,
			en => enRegFile(2), d => result, q => dataPTR2 );

	PTR3: REG PORT MAP ( clk => clk, res => res,
			en => enRegFile(3), d => result, q => dataPTR3 );

	PSW: PswREG PORT MAP ( clk => clk, res => res, enData => enRegFile(4),
			enFlags => enFlags, vcnz => vcnz, d => result, q => dataPSW );

	IEN: IenREG PORT MAP ( clk => clk, res => res, enData => enRegFile(5), 
			enGIE => enGIE, GIEflag => GIEflag, d => result, q => dataIEN );
			
	IFLAG: IFlagREG PORT MAP ( clk => clk, res => res, resIFLAG => resIFLAG, 
			en => enRegFile(6), interrupts => interrupts, d => result, q => dataIFLAG );
			
   AL: ALU PORT MAP( A => dataACC, B => dataB, C => result, ALUFunc => ALUFunc, 
			ov => vcnz(3), cout => vcnz(2), n => vcnz(1), z => vcnz(0),
			nBit => nsd(2), shiftrot => nsd(1),	dir => nsd(0));
			 
	CU: controlUnit 
		PORT MAP ( clk => clk, res => res, oe => oe, opcode => opcode, flags => flags, wren => wren, 
			enReg => enReg, enPC => enPC, enIR => enIR, enACC => enACC, enFlags => enFlags, enGIE => enGIE,
			ALUfunc => ALUfunc, nsd => nsd, GIEflag => GIEflag, resIFLAG => resIFLAG, addrINT => addrINT,
			selB => selB, selAddr => selAddr, selPC => selPC, selReg => selReg, IFLAG => dataIFLAG(7 DOWNTO 0), 
			IEN => dataIEN(8 DOWNTO 0));
	
	-- opcode extraction: function @ IR[15:12], sub-funtion @ IR[9:8] and IR[5:4], selReg @ [2:0] 
	opcode <= dataIR(15 DOWNTO 12) & dataIR(9 DOWNTO 8) & dataIR (5 DOWNTO 4) & dataIR(2 DOWNTO 0);
	
	-- n & z used in bz and bn instructions
	flags <= dataPSW(1 DOWNTO 0);
	
	-- output data to be stored
	dataO <= dataACC;	
	
	-- PC input multiplexer (0: address from ALU result, 1: interrupt address)
	nextAddr <= result WHEN selPC = '0' ELSE (15 DOWNTO 3 => '0') & addrINT;
	
	-- address bus multiplexer (0: IR, 1: PC, 2: RegFile, 3: nextAddr)
	WITH selAddr SELECT address <= 
		dataIR(11 DOWNTO 0) WHEN "00", 
		dataPC(11 DOWNTO 0) WHEN "01",
		dataReg(11 DOWNTO 0) WHEN "10",
		nextAddr(11 DOWNTO 0) WHEN OTHERS; 

	-- ALU data multiplexer (0: IR, 1: PC, 2: memory, 3: RegFile)
	WITH selB SELECT dataB <=
		X"0" & dataIR(11 DOWNTO 0) WHEN "00",
		dataPC WHEN "01",
		dataI WHEN "10",
		dataReg WHEN OTHERS;
	
	-- register file multiplexer
	WITH selReg SELECT dataReg <=
		dataSP WHEN "000",
		dataPTR1 WHEN "001",
		dataPTR2 WHEN "010",
		dataPTR3 WHEN "011",
		dataPSW WHEN "100",
		dataIEN WHEN "101",
		dataIFLAG WHEN "110",
		(OTHERS => '0') WHEN OTHERS;

	-- register enable generation (SP, PTR1, PTR2, PTR3, PSW, IEN)
	EnGen: FOR i IN 0 TO 6 GENERATE
		enRegFile(i) <= enReg WHEN i = TO_INTEGER(UNSIGNED(selReg)) ELSE '0';
	END GENERATE;

END structure;
