--------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 22.06.2019 18:27:31
-- Design Name: UKM910 Processor
-- Module Name: VGA_buff - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: IEEE.NUMERIC_STD
-- 
-- Dependencies: 
-- 
-- Revision: 6
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;


ENTITY vga_buff IS
    PORT ( 
        clk : IN STD_LOGIC;
        res : IN STD_LOGIC;

		  -- input to vga driver
        vga_data : OUT STD_LOGIC_VECTOR (10 DOWNTO 0);
		  
		  -- output from vga_mem
        mem_data : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		  
		  -- buffer control from vga driver
        buff_addr_vga : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        
		  -- vga memory control from buffer controller
        mem_wren_vga : OUT STD_LOGIC;   
        mem_addr_vga : OUT STD_LOGIC;
        
		  -- vga memory control from processor
        mem_wren_proc : IN STD_LOGIC;
        mem_oe_proc : IN STD_LOGIC
    );
END vga_buff;

ARCHITECTURE Behavioral OF vga_buff IS

-- component decleration

COMPONENT buff IS
	PORT (
		clk : IN STD_LOGIC;
		wren : IN STD_LOGIC;
		rdAddr : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		wrAddr : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		dataI : IN STD_LOGIC_VECTOR (10 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (10 DOWNTO 0)
    );
END COMPONENT;

TYPE state IS (busy, requestFlags, checkFlags, requestAddr, requestData, updateBuff);

-- signal deceration

SIGNAL currentState, nextState : state;

SIGNAL buff_wren : STD_LOGIC := '0';
SIGNAL buff_addr_proc : STD_LOGIC_VECTOR (11 DOWNTO 0) := (OTHERS => '0');
SIGNAL buff_dataI : STD_LOGIC_VECTOR (10 DOWNTO 0) := (OTHERS => '0');

SIGNAL flags : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL pre_flags : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL pre_buff_dataI : STD_LOGIC_VECTOR (10 DOWNTO 0) := (OTHERS => '0');
SIGNAL pre_buff_addr_proc : STD_LOGIC_VECTOR (11 DOWNTO 0) := (OTHERS => '0');

BEGIN		-- Behavioral

	-- component installation
	
	BUFF0: buff 
		PORT MAP (
			clk => clk,
			wren => buff_wren, 
			rdAddr => buff_addr_vga, 
			wrAddr => buff_addr_proc,
			dataI => buff_dataI,
			dataO => vga_data
		);
	
	PROCESS (clk, res) IS   -- FSM
	BEGIN
    
		IF (res = '1') THEN
			currentState <= busy;		-- vga_mem access disabled
            
		ELSIF (clk'EVENT AND clk = '1') THEN
         
			-- registers to hold flags, address and data
			pre_flags <= flags;   
			pre_buff_dataI <= buff_dataI;
			pre_buff_addr_proc <= buff_addr_proc;
         
			-- buffer is not busy and processor is not using vga_mem
			IF (mem_wren_proc = '0' AND mem_oe_proc = '0') THEN
				currentState <= nextState;		-- internal transfer
         
			-- processor is reading or writing in vga_mem			
			ELSE
				currentState <= busy;	-- vga_mem access disabled
                
			END IF;
		END IF;
	END PROCESS;

	PROCESS (currentState, flags, mem_data, pre_buff_dataI, pre_buff_addr_proc, pre_flags) IS   
	BEGIN

		-- default values
		
		buff_addr_proc <= pre_buff_addr_proc;		-- no change
		buff_dataI <= pre_buff_dataI;					-- no change
		flags <= pre_flags;								-- no change
      
		mem_addr_vga <= '0';			-- flag address
		mem_wren_vga <= '0';			-- vga_mem write disabled
      buff_wren <= '0';				-- buffer write disabled					
      
		-- next state and output generation
		
		CASE (currentState) IS
     
			-- memory access disabled
			WHEN busy=>
				nextState <= requestFlags;
         
			-- read the flags from vga_mem			
			WHEN requestFlags =>
				nextState <= checkFlags;
				flags <= mem_data;
        
		   -- check the flags / obtain buff address
			WHEN checkFlags =>
				-- 0: no new charachters, 
				IF (flags(15) = '0') THEN
					nextState <= requestFlags;
				-- 1: new characters available
				ELSE
					nextState <= requestData;
					buff_addr_proc <= flags(11 DOWNTO 0);
					mem_addr_vga <= '1';			-- char ascii address in vga_mem	
				END IF;
         
			-- read the charachter ascii code and update buffer
			WHEN requestData =>
				nextState <= requestFlags;
				buff_dataI(10 DOWNTO 8) <= flags(14 DOWNTO 12);		-- color flags
				buff_dataI(7 DOWNTO 0) <= mem_data(7 DOWNTO 0);		-- read char ascii from vga_mem
				buff_wren <= '1';												-- enable buffer write (store char at buff addr)
				mem_wren_vga <= '1';											-- enable vga_mem write to reset flags (store 0 at flags addr)
            
			WHEN OTHERS => 
				nextState <= busy;
				
		END CASE;
	END PROCESS;

	-- buffer address is obtained from vga controller when internal transfer is in progress (transfering data from vga_mem to buffer). 
	-- buffer address is obtained from vga driver when it read the next character to display.
   
END Behavioral;
