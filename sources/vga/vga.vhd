----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: 
-- 
-- Create Date: 14.05.2019 14:15:39
-- Design Name: UKM910 Processor
-- Module Name: VGA - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
-- 
-- Dependencies: IEEE.NUMERIC_STD
-- 
-- Revision: 7
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY IEEE;
LIBRARY WORK;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE WORK.fontROM.ALL; 

ENTITY vga IS
	PORT ( 
		-- control signals
		clk : IN  STD_LOGIC;
		res : IN STD_LOGIC;
		
		-- buffer signals
		buff_addr: OUT STD_LOGIC_VECTOR(11 downto 0);
		buff_data: IN STD_LOGIC_VECTOR(10 downto 0);
		
		-- vga port signals
		vsync : OUT STD_LOGIC;
		hsync : OUT STD_LOGIC;
		red   : OUT STD_LOGIC;
		green : OUT STD_LOGIC;
		blue  : OUT STD_LOGIC
	);
END vga;

ARCHITECTURE Behavioral OF vga IS

-- signal declaration
SIGNAL pxl_clk : std_logic:='0';

SIGNAL hsync_pulse : INTEGER RANGE 0 TO 800;
SIGNAL vsync_pulse : INTEGER RANGE 0 TO 524;

SIGNAL char_height_counter : INTEGER RANGE 0 TO 11;
SIGNAL char_width_counter : INTEGER RANGE 0 TO 7;
SIGNAL char_in_line_offset : INTEGER RANGE 0 TO 39*80-1;
SIGNAL char_in_line_counter : INTEGER RANGE 0 TO 79;

SIGNAL fontRom_offset : INTEGER RANGE 0 TO 11*256-1;
SIGNAL frontRom_data : STD_LOGIC_VECTOR (7 DOWNTO 0);

SIGNAL pixel_offset : INTEGER RANGE 0 TO 7;
SIGNAL pixel_color : STD_LOGIC_VECTOR (2 DOWNTO 0);

BEGIN

	-- PS: All timings are obtained from the project documentation provided on Ilias 	
	-- and the lecture at http://www.eng.ucy.ac.cy/theocharides/Courses/ECE664/VGA.pdf
	
	-- pixel clock generation (25 MHz)
	PROCESS (clk)
	BEGIN
		-- clock divider (divides by 2)
		IF (clk'EVENT AND clk = '1') THEN
			pxl_clk <= NOT pxl_clk;  
		END IF;
	END PROCESS;

	-- horizontal and vertical counters
	PROCESS (pxl_clk, res)
	BEGIN
		IF (res = '1') THEN
			hsync_pulse <= 0;	
			vsync_pulse <= 0;

		ELSIF (pxl_clk' EVENT AND pxl_clk = '1') THEN
			-- hsync pulse < 800 pixel clock pulses
			IF (hsync_pulse < 799) THEN
				hsync_pulse <= hsync_pulse + 1;
			-- hsync pulse = 800 pixel clock pulses
			ELSE
				-- reset hsync pulse 
				hsync_pulse <= 0;
				-- vsync pulse < 525 pixel clock pulses
				IF (vsync_pulse < 524) THEN
					vsync_pulse <= vsync_pulse + 1;
				-- vsync pulse = 525 pixel clock pulses
				ELSE
					-- reset vsync pulse
					vsync_pulse <= 0;
				END IF;				
			END IF; 
		END IF;
	END PROCESS;

	-- hsync and vsync generation
	PROCESS (pxl_clk, res)
	BEGIN
		-- switch off on reset
		IF (res = '1') THEN
			hsync <= '0';
			vsync <= '0';
		-- pulse level generation
		ELSIF (pxl_clk'EVENT AND pxl_clk = '1') THEN
			-- horizontal: T_disp = 640, T_fp = 16, T_p = 96
			-- hsync [T_disp + T_fp : T_disp + T_fp + T_p] = 0
			IF (hsync_pulse > 656 AND hsync_pulse < 752) THEN
				hsync <= '0';
			-- hsync = 1 otherwise
			ELSE
				hsync <= '1';
			END IF;
			-- vertical: T_disp = 480, T_fp = 10, T_p = 2
			-- vsync [T_disp + T_fp : T_disp + T_fp + T_p] = 0      
			IF (vsync_pulse > 490 AND vsync_pulse < 492) THEN
				vsync <= '0';
			-- vsync = 1 otherwise
			ELSE
				vsync <= '1';
			END IF;			
		END IF;
	END PROCESS;

	-- character pixel position counters
	PROCESS (pxl_clk, res)
	BEGIN
		-- reset all counters on reset
		IF (res = '1') THEN
			char_in_line_counter <= 0;
			char_in_line_offset <= 0;
			char_width_counter <= 0;
			char_height_counter <= 0;	
			fontRom_offset <= 0;
		
		ELSIF (pxl_clk'EVENT AND pxl_clk = '1') THEN
			-- within frame width
			IF (hsync_pulse < 640) THEN 
				-- char_width_counter: one charachter has 8 horizontal pixels 
				char_width_counter <= char_width_counter + 1;	
				-- horizontal end of a charachter
				IF (char_width_counter = 7) THEN
					char_width_counter <= 0;
					-- char_in_line_counter: one line has 80 charachters
					char_in_line_counter <= char_in_line_counter + 1;
				END IF;
			ELSE
				char_in_line_counter <= 0;
				char_width_counter <= 0;
			END IF;
			-- end of frame width
			IF (hsync_pulse = 640) THEN
				-- within frame height
				IF (vsync_pulse < 480) THEN
					-- char_height_counter: one charachter has 12 vertical pixels
					char_height_counter <= char_height_counter + 1;
					-- fontRom_offset: 12 addresses/charachter for 256 charachters
					fontRom_offset <= fontRom_offset + 256;
					-- vertical end of a charachter
					IF (char_height_counter = 11) THEN
						-- char_in_line_offset: number of complete lines x 80 charachters/line
						char_in_line_offset <= char_in_line_offset + 80;
						char_height_counter <= 0;
						fontRom_offset <= 0;
					END IF;
				ELSE
					char_height_counter <= 0;
					char_in_line_offset <= 0;
					fontRom_offset <= 0;
				END IF;
			END IF;
		END IF;
	END PROCESS;
	
	-- rgb signal generation
	PROCESS (pxl_clk, res)
	BEGIN
		-- switch off on reset
		IF (res = '1') THEN	
			red <= '0';
			green <= '0';
			blue <= '0';
			
		ELSIF (pxl_clk'EVENT AND pxl_clk = '1') THEN
			-- delay by one cycle (to synchronize with vga memory)
			pixel_offset <= char_width_counter;
			-- counters are within the frame size
			IF (hsync_pulse < 640 OR vsync_pulse < 480) THEN
				-- buffer address = chars in current line + 80 chars/line * no. of full lines
				buff_addr <= STD_LOGIC_VECTOR(TO_UNSIGNED((char_in_line_counter + char_in_line_offset), 12));
				-- rgb = rom pixel data when the color is enabled
				red <= frontRom_data(pixel_offset) AND pixel_color(2);
				green <= frontRom_data(pixel_offset) AND pixel_color(1);
				blue <= frontRom_data(pixel_offset) AND pixel_color(0);	
			ELSE
				red <= '0';
				green <= '0';
				blue <= '0';
			end if;
		end if;
	end process;
   
	-- fontRom address = character code + character offset in fontRom
	frontRom_data <= fontRom(TO_INTEGER(UNSIGNED(buff_data(7 DOWNTO 0))) + fontRom_offset);
	-- pixel colors are stored in the 3 MSB of the buffer
	pixel_color <= buff_data(10 DOWNTO 8);
	
end Behavioral;
