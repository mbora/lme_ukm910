----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 23.06.2019 00:12:22
-- Design Name: UKM910 Processor
-- Module Name: buffer - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
-- 
-- Dependencies: IEEE.NUMERIC_STD
-- 
-- Revision: 0
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY buff IS
    PORT (
        clk : IN STD_LOGIC;
        wren : IN STD_LOGIC;
        wrAddr : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		  rdAddr : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        dataI: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
        dataO: OUT STD_LOGIC_VECTOR (10 DOWNTO 0)
    );
END buff;
 
ARCHITECTURE Behavioral OF buff IS

TYPE data_array IS ARRAY (0 TO 2**12-1) OF STD_LOGIC_VECTOR (10 DOWNTO 0);

-- intialize the buffer with zeros
SIGNAL buff_data : data_array :=  (OTHERS => (OTHERS => '0'));
SIGNAL read_addr : STD_LOGIC_VECTOR (11 DOWNTO 0) := (OTHERS => '0');

BEGIN

	PROCESS (clk) IS
   BEGIN
    
		IF (clk'EVENT AND clk = '1') THEN	-- synchronous (rising edge)
         read_addr <= rdAddr;
			IF (wren = '1') THEN		-- write enable
            
				buff_data(TO_INTEGER(UNSIGNED(wrAddr))) <= dataI;
                
			END IF;
		END IF;
	END PROCESS;
    
	-- asynchronous read
   dataO <= buff_data(TO_INTEGER(UNSIGNED(read_addr)));
     
END Behavioral;
