--------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 22.06.2019 18:16:12
-- Design Name: UKM910 Processor
-- Module Name: VGA_top - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
--
-- Dependencies: IEEE.NUMERIC_STD
--
-- Revision: 9
-- Revision 0.01 - File Created
-- ******************************************************
-- Additional Comments: READ THE PROTOCOL FOR MORE INFO
-- ******************************************************
--
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY VGA_HW IS
    Port (  
        clk : IN  STD_LOGIC;
        res: IN  STD_LOGIC;
             
        oe : IN STD_LOGIC; 
        wren : IN STD_LOGIC;  
        
        addr : IN STD_LOGIC;
        dataI : IN STD_LOGIC_VECTOR (15 downto 0);
        dataO: OUT STD_LOGIC_VECTOR (15 downto 0);
        
        vsync : OUT STD_LOGIC;
        hsync : OUT STD_LOGIC;
        
        red : OUT STD_LOGIC;
        green : OUT STD_LOGIC;
        blue : OUT STD_LOGIC
    );
END VGA_HW;

ARCHITECTURE structure OF VGA_HW IS

-- component declaration

COMPONENT vga IS 
	PORT ( 
		clk : IN  STD_LOGIC;
		res : IN STD_LOGIC;
		
		buff_addr: OUT STD_LOGIC_VECTOR(11 downto 0);
		buff_data: IN STD_LOGIC_VECTOR(10 downto 0);
				
		vsync : OUT STD_LOGIC;
		hsync : OUT STD_LOGIC;

		red   : OUT STD_LOGIC;
		green : OUT STD_LOGIC;
		blue  : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT vga_buff IS 
	PORT ( 
		clk : IN STD_LOGIC;
		res : IN STD_LOGIC;
         
		buff_addr_vga : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
                  
		mem_wren_vga : OUT STD_LOGIC;   
		mem_addr_vga : OUT STD_LOGIC;
         
		mem_wren_proc : IN STD_LOGIC;
		mem_oe_proc : IN STD_LOGIC;
         
		vga_data : OUT STD_LOGIC_VECTOR (10 DOWNTO 0);
		mem_data : IN STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT vga_mem IS
    Port ( 
		clk : IN STD_LOGIC;
		wren : IN STD_LOGIC;
		addr : IN STD_LOGIC;
		dataI : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
    );
END COMPONENT; 

-- signal declaration
   
SIGNAL  buff_addr_vga : STD_LOGIC_VECTOR (11 DOWNTO 0);

SIGNAL  vga_dataI : STD_LOGIC_VECTOR (10 DOWNTO 0) := (OTHERS => '0');
SIGNAL  mem_dataI : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS =>'0');
SIGNAL  mem_dataO : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS =>'0');

SIGNAL  mem_address : STD_LOGIC := '0';
SIGNAL  mem_addr_vga : STD_LOGIC := '0';
SIGNAL  mem_addr_proc : STD_LOGIC := '0';

SIGNAL  mem_wren : STD_LOGIC := '0';
SIGNAL  mem_wren_vga : STD_LOGIC := '0';
SIGNAL  mem_wren_proc : STD_LOGIC := '0';

BEGIN		-- structure

	-- component installation
	
	VGA_DRIVER: vga
		PORT MAP (
			clk => clk,
			res => res,
         vsync => vsync,
			hsync => hsync,
         red => red,
         green => green,
         blue => blue,
         buff_data => vga_dataI,
         buff_addr => buff_addr_vga
		);
    
	VGA_BUFFER: VGA_buff 
		PORT MAP (
			clk => clk,
         res => res,
         buff_addr_vga => buff_addr_vga,
         mem_wren_vga => mem_wren_vga,
         mem_addr_vga => mem_addr_vga,
         mem_wren_proc => wren,
         mem_oe_proc => oe,
         vga_data => vga_dataI,
         mem_data => mem_dataO
		);
 
	VGA_MEMORY: VGA_MEM 
		PORT MAP (
			clk => clk,
         wren => mem_wren,
         addr => mem_address,
         dataI => mem_dataI,
         dataO => mem_dataO
      );    
    
	 -- component specific signal generation
    mem_wren_proc <= wren;		-- just for clearification
    mem_addr_proc <= addr;		-- just for clearification
    
	 -- vga memory can be written by processor or vga buffer controller *
    mem_address <= mem_addr_proc when mem_wren_proc = '1' ELSE mem_addr_vga;    
    mem_dataI <= dataI when mem_wren_proc = '1' ELSE X"0000" ;           
    mem_wren <= mem_wren_proc OR mem_wren_vga;
    
	 -- data out corresponds to memory*
    dataO <= mem_dataO;
    
END structure;

-- * in order to display a charachter, the processor first reads the busy flag. if it was 0
--   the processor writes the (ascii of the charachter) and (its position on the display) to 
--   vga_memory and sets the busy flag to 1. when the flag is set, the vga buffer controller
--	  starts transfering the data from vga_memory to the buffer. during this internal transfer,
--   the vga hardware output should be X"0001" so that the processor finds the HW busy in case
--	  it reads the busy flag to display a new character while a previous one is being processed.
--   after the transfer in done, the vga controller resets the busy flag.
--   check the protocol for more information