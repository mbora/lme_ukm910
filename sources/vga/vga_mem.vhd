----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Kareem Mansour & Monideep Bora
-- 
-- Create Date: 22.06.2019 20:06:07
-- Design Name: UKM910 Processor
-- Module Name: vga_mem - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
-- 
-- Dependencies: IEEE.NUMERIC_STD
-- 
-- Revision: 0
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY vga_mem IS
    Port ( 
        clk : IN STD_LOGIC;
        wren : IN STD_LOGIC;
        addr : IN STD_LOGIC;
        dataI : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
        dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
    );
END vga_mem;

ARCHITECTURE Behavioral OF vga_mem IS

-- 00: flag, 01: buffer address, 10: character ASCII
TYPE data_array IS ARRAY (0 TO 1) OF STD_LOGIC_VECTOR (15 DOWNTO 0);

-- signal declaration

SIGNAL mem_data : data_array :=  (OTHERS => (OTHERS => '0'));
SIGNAL read_addr : STD_LOGIC := '0';

BEGIN		-- Behavioral

	PROCESS (clk) IS
	BEGIN
	
	IF (clk'EVENT AND clk = '1') THEN	
		-- address is changed only at rising edge
		read_addr <= addr;
          -- write enabled      
			IF (wren = '1') THEN      
				IF (addr = '0') THEN
					mem_data(0) <= dataI;
				ELSE
					mem_data(1) <= dataI;     
				END IF;
			END IF;
		END IF;
	END PROCESS;

	-- asynchronus read from synchronous address!
	dataO <= mem_data(0) WHEN read_addr = '0' ELSE mem_data(1);
    
END Behavioral;
