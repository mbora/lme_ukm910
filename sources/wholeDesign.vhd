----------------------------------------------------------------------------------
-- Company: Freiburg University
-- Engineer: Group 4 (Kareem, Konstantin, Monideep)
-- 
-- Create Date: 06/23/2019 08:39:17 PM
-- Design Name: UKM910 Processor
-- Module Name: wholeDesign - Behavioral
-- Project Name: VLSI Course Project
-- Target Devices: Spartan-3E Satarter Kit
-- Tool versions: 14.7
-- Description: 
-- 
-- Dependencies: IEEE.NUMERIC_STD
-- 
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;


ENTITY wholeDesign IS
	PORT ( 
		-- control signals
		clk : IN STD_LOGIC;
		res : IN STD_LOGIC;
		-- inputs
--		interrupts : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		PS2clk : IN STD_LOGIC;
		PS2data : IN STD_LOGIC;
		-- outputs
		vsync : OUT STD_LOGIC;
		hsync : OUT STD_LOGIC;
		red : OUT STD_LOGIC;
		green : OUT STD_LOGIC;
		blue : OUT STD_LOGIC	
	);
END wholeDesign;

ARCHITECTURE structure OF wholeDesign IS

-- component decleration

COMPONENT UKM910 IS
	PORT ( 
		clk : IN  STD_LOGIC;
		res : IN  STD_LOGIC;
		dataI : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		address : OUT  STD_LOGIC_VECTOR (11 DOWNTO 0);
		interrupts : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		wren : OUT  STD_LOGIC;
		oe : OUT  STD_LOGIC
	);
END COMPONENT;

COMPONENT MEMORY is
	PORT ( 
		clk, wren: IN  STD_LOGIC;
		address : IN  STD_LOGIC_VECTOR (10 DOWNTO 0);
		dataI : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT  STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT VGA_HW IS
	PORT (  
		clk : IN  STD_LOGIC;
		res: IN  STD_LOGIC;      
		oe : IN STD_LOGIC; 
		wren : IN STD_LOGIC;  
		addr : IN STD_LOGIC;
		dataI : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		dataO : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		vsync : OUT STD_LOGIC;
		hsync : OUT STD_LOGIC;
		red : OUT STD_LOGIC;
		green : OUT STD_LOGIC;
		blue : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT ps2_hw IS
	PORT (
		clk : IN STD_LOGIC;
		reset : IN STD_LOGIC;  
		oen : IN STD_LOGIC; 
		ps2_clk : IN STD_LOGIC;
		ps2_data : IN STD_LOGIC;
		irq : OUT STD_LOGIC;
		dataOut : OUT STD_LOGIC_VECTOR(15 downto 0)
	);
END COMPONENT;

-- signal decleration

SIGNAL address_proc : STD_LOGIC_VECTOR (11 DOWNTO 0) := (OTHERS => '0');
SIGNAL data_proc : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL oe_proc : STD_LOGIC := '0';
SIGNAL wren_proc : STD_LOGIC := '0';

SIGNAL data_mem : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL oe_mem : STD_LOGIC := '0';
SIGNAL wren_mem : STD_LOGIC := '0';
SIGNAL sel_mem : STD_LOGIC := '0';

SIGNAL data_vga : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL wren_vga : STD_LOGIC := '0';
SIGNAL oe_vga : STD_LOGIC := '0';
SIGNAL sel_vga : STD_LOGIC := '0';

SIGNAL data_ps2 : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL oe_ps2 : STD_LOGIC := '0';
SIGNAL sel_ps2 : STD_LOGIC := '0';
SIGNAL iqr_ps2 : STD_LOGIC := '0';

SIGNAL data_bus : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
SIGNAL sel_data : STD_LOGIC_VECTOR (1 DOWNTO 0) := (OTHERS => '0');

SIGNAL interrupts : STD_LOGIC_VECTOR (7 DOWNTO 0);

BEGIN		-- structure

	-- component installation
	
	UKM910_1 : UKM910 
		PORT MAP (
			clk => clk,
			res => res,
			interrupts => interrupts,
			address => address_proc,
			dataI => data_bus,
			dataO => data_proc,
			wren => wren_proc,
			oe => oe_proc
	);

	MEMORY_1 : MEMORY
		PORT MAP (
			clk => clk,
			address => address_proc(10 DOWNTO 0),
			dataI => data_proc,
			dataO => data_mem,
			wren => wren_mem
		);
    
	VGA_HW_1 : VGA_HW 
		PORT MAP (
			clk => clk,
         res => res,
         oe => oe_vga,
         wren => wren_vga,
         addr => address_proc(0),
         dataI => data_proc,
         dataO => data_vga,
         vsync => vsync,
         hsync => hsync,
         red => red,
         blue => blue,
         green => green
		);
 
	PS2_HW_1 : PS2_HW
		PORT MAP (
			clk => clk,
			reset => res,
			oen => oe_ps2,
			ps2_clk => PS2clk,
			ps2_data => PS2data,
			irq => iqr_ps2,
			dataOut => data_ps2
		);
	
	-- component specific signal generation
	
	sel_mem <= '1' WHEN address_proc(11) = '0' ELSE '0';
	sel_vga <= '1' WHEN address_proc(11 DOWNTO 1) = (11 DOWNTO 1 => '1') ELSE '0';
	sel_ps2 <= '1' WHEN address_proc = X"FFB" ELSE '0';

	oe_mem <= sel_mem AND oe_proc;
	oe_vga <= sel_vga AND oe_proc;  
	oe_ps2 <= sel_ps2 AND oe_proc;
	
	wren_mem <= sel_mem and wren_proc;
	wren_vga <= sel_vga and wren_proc; 

	-- delay dataBus selection by 1 cycle *
	PROCESS (clk)
	BEGIN
		IF (clk'EVENT AND clk = '1') THEN
			IF (sel_mem = '1') THEN
				sel_data <= "01";
			ELSIF (sel_vga = '1') THEN
				sel_data <= "10";
			ELSIF (sel_ps2 = '1') THEN
				sel_data <= "11";
			ELSE
				sel_data <= "00";
			END IF;
		END IF;
	END PROCESS;
	
	-- dataBus source multiplexer
	WITH sel_data SELECT data_bus <= 
		data_mem WHEN "01",
		data_vga WHEN "10",
		data_ps2 WHEN "11",
		(OTHERS => '0') WHEN OTHERS;
	
	interrupts <= (1 => iqr_ps2, OTHERS => '0');
	
END structure;

-- * The address has to be set in one cycle and the data will be available after the next clock rising edge.
--   Therefore the data selection has to be delayed by one cyle in order to correspond to the address that 
--	  was used in the previous cycle.