
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name VLSI_Project -dir "C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/planAhead_run_1" -part xc3s500efg320-4
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/wholeDesign.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project} }
set_property target_constrs_file "C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/sources/wholeDesign.ucf" [current_fileset -constrset]
add_files [list {C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/sources/wholeDesign.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/wholeDesign.ncd"
if {[catch {read_twx -name results_1 -file "C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/wholeDesign.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"C:/Users/Kareem/Documents/VLSI System Design/Project/VLSI_Project/wholeDesign.twx\": $eInfo"
}
